/*
 * Copyright 2008-2009 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

/*!
 * @file drivers/hwmon/sht21.c
 *
 * @brief sht21 i2c humidity sensor Driver
 *
 * @ingroup
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/hwmon.h>
#include <linux/hwmon-sysfs.h>
#include <linux/ctype.h>
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/regulator/consumer.h>
#include <linux/workqueue.h>
#include <mach/hardware.h>

struct sht21_data {
	struct i2c_client *client;
	struct device *hwmon_dev;
	unsigned char enable;
};

static struct i2c_client *sht21_client;

static void read_hum_wq(struct work_struct *work);
DECLARE_WORK(read_hum_work, read_hum_wq);
static int hum = -2;

#define WQ_IDLE -3
#define WQ_RUNNING -2
#define HUM_ERROR -1


static int sht21_readUserRegister(void)
{
	unsigned char msgbuf0;
	unsigned char msgbuf1;
	int status;

	struct sht21_data *data = i2c_get_clientdata(sht21_client);
	struct i2c_msg msg[2] = { { sht21_client->addr, sht21_client->flags				, 1, &msgbuf0 },
	                          { sht21_client->addr, sht21_client->flags | I2C_M_RD	, 1, &msgbuf1 }
	                        };

	if (!(data->enable))
		return -ENODEV;

	msgbuf0 = 0xe7;	// read register
	
	status = i2c_transfer(sht21_client->adapter, msg, 2);
	if (status < 0)
		return status;

	return ((int)msgbuf1);
}

static int sht21_writeUserRegister(unsigned char reg)
{
	unsigned char msgbuf0[2];
	int status;

	struct sht21_data *data = i2c_get_clientdata(sht21_client);
	struct i2c_msg msg[1] = { { sht21_client->addr, sht21_client->flags, 2, msgbuf0 }
					};

	if (!(data->enable))
		return -ENODEV;

	msgbuf0[0] = 0xe6;	// write register
	msgbuf0[1] = reg;	// register data
	
	status = i2c_transfer(sht21_client->adapter, msg, 1);
	if (status < 0)
		return status;

	return (0);
}

static int sht21_MeasurePoll(void)
{
	unsigned char msgbuf0;
	unsigned char msgbuf1[4];
	int status;
	unsigned char c;

	struct sht21_data *data = i2c_get_clientdata(sht21_client);
	struct i2c_msg msg[2] = { { sht21_client->addr, sht21_client->flags				, 1, &msgbuf0 },
	                          { sht21_client->addr, sht21_client->flags | I2C_M_RD	, 3, &msgbuf1 }
	                        };


	if (!(data->enable))
		return -ENODEV;

	msgbuf0 = 0xf5;	// trigger humidity measurement 
	
	status = i2c_transfer(sht21_client->adapter, msg, 2);

	c=0;
	do
	{
		msleep(10);
		status = i2c_transfer(sht21_client->adapter, &msg[1], 1);
		if (status < 0)
		{
			c++;
		}
		else
		{
			c = 99;
		}
	}
	while (c<20);
	if (c == 20)
	{
		msgbuf1[0] = 0;	// return -6 if conversion times out
		msgbuf1[1] = 0;
	}
	
	msgbuf1[1] &= 0xfc; // mask out status bits
	
	hum = ((125*((256*msgbuf1[0])+msgbuf1[1]))>>16)-6;
	return (hum);
}

static void read_hum_wq(struct work_struct *work)
{
	struct sht21_data *client_data = i2c_get_clientdata(sht21_client);

	sht21_MeasurePoll();
	sysfs_notify(&client_data->client->dev.kobj, NULL, "humidity");
}

static ssize_t show_hum(struct device *dev, struct device_attribute *attr,
			char *buf)
{
	ssize_t buf_size;

	if (hum == WQ_IDLE)
	{
		*buf = '\0';
		schedule_work(&read_hum_work);
		return 0;
	}
	else if (hum == WQ_RUNNING)
	{
		*buf = '\0';
		return 0;
	}
	else
	{
		buf_size = snprintf(buf, PAGE_SIZE, "%u\n", hum);
		hum = WQ_IDLE;
		return buf_size;
	}
}

static SENSOR_DEVICE_ATTR(humidity, S_IRUGO, show_hum, NULL, 0);


static int sht21_i2c_probe(struct i2c_client *client,
			      const struct i2c_device_id *did)
{
	int err = 0;
	struct sht21_data *data;
	int reg = 0;

	sht21_client = client;
	data = kzalloc(sizeof(struct sht21_data), GFP_KERNEL);
	if (data == NULL) {
		err = -ENOMEM;
		goto exit1;
	}

	i2c_set_clientdata(client, data);
	data->client = client;
	
	data->enable = 1;

	reg = sht21_readUserRegister();
	
	if (reg != 0x3a)
	{
		err = -ENODEV;
		goto exit2;
	}

	err = device_create_file(&client->dev, &sensor_dev_attr_humidity.dev_attr);
	if (err)
		goto exit2;

//  Register sysfs hooks
	data->hwmon_dev = hwmon_device_register(&client->dev);
	if (IS_ERR(data->hwmon_dev)) {
		err = PTR_ERR(data->hwmon_dev);
		goto exit_remove;
	}

	hum = WQ_IDLE;
	
	return 0;

exit_remove:
	device_remove_file(&client->dev, &sensor_dev_attr_humidity.dev_attr);
exit2:
	kfree(data);
exit1:
	sht21_client = NULL;
	return err;
}

static int sht21_i2c_remove(struct i2c_client *client)
{
	struct sht21_data *data = i2c_get_clientdata(client);

	hwmon_device_unregister(data->hwmon_dev);
	device_remove_file(&client->dev, &sensor_dev_attr_humidity.dev_attr);
	kfree(data);
	return 0;
}

static int sht21_suspend(struct i2c_client *client, pm_message_t message)
{
	return 0;
}

static int sht21_resume(struct i2c_client *client)
{
	return 0;
}

static const struct i2c_device_id sht21_id[] = {
	{"sht21", 0},
	{}
};
MODULE_DEVICE_TABLE(i2c, sht21_id);

static struct i2c_driver sht21_driver = {
	.driver = {
		   .name = "sht21",
		   },
	.probe = sht21_i2c_probe,
	.remove = sht21_i2c_remove,
	.suspend = sht21_suspend,
	.resume = sht21_resume,
	.id_table = sht21_id,
};

static int __init sht21_init(void)
{
	return i2c_add_driver(&sht21_driver);;
}

static void __exit sht21_cleanup(void)
{
	i2c_del_driver(&sht21_driver);
}

module_init(sht21_init);
module_exit(sht21_cleanup);

MODULE_AUTHOR("Klaus Steinhammer <kst@tttech.com>");
MODULE_DESCRIPTION("sht21 i2c humidity sensor driver");
MODULE_LICENSE("GPL");
