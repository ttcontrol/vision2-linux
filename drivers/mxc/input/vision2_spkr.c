/*
 *  Vision2 beeper driver for Linux
 *
 *  Copyright (c) 2010 Klaus Steinhammer
 *  Copyright (c) 2002 Vojtech Pavlik
 *  Copyright (c) 1992 Orest Zborowski
 *
 */

/*
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/input.h>
#include <linux/platform_device.h>
#include <linux/timex.h>
#include <linux/hrtimer.h>
#include <linux/slab.h>
#include <asm/io.h>
#include <mach/gpio.h>
#include <mach/hardware.h>
#include "../../../arch/arm/mach-mx5/iomux.h"
#include "../../../arch/arm/mach-mx5/mx51_pins.h"

MODULE_AUTHOR("Klaus Steinhammer <kst@tttech.com>");
MODULE_DESCRIPTION("Vision2 beeper driver");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:pcspkr");

static int current_value = 0;

struct pcspkr_state {
	struct input_dev *pcspkr_dev;
	int value;
};


static int pcspkr_event(struct input_dev *dev, unsigned int type, unsigned int code, int value)
{
	unsigned int count = 0;
//	struct pcspkr_state *spkr_state = input_get_drvdata(dev);

	if (type != EV_SND)
		return -1;

	switch (code) {
		case SND_BELL: if (value) value = 1000;
		case SND_TONE: break;
		default: return -1;
	}

	if ( value > 0 )
	{
		gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_NANDF_D14),1);
	}
	else
	{
		gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_NANDF_D14),0);
	}

        current_value = value;

//	printk(KERN_INFO "beeper: on %d value %d\n", count, value);

	return 0;
}

static int __devinit pcspkr_probe(struct platform_device *dev)
{
	struct input_dev *pcspkr_dev;
	int err;
	struct pcspkr_state *spkr_state;

	pcspkr_dev = input_allocate_device();
	if (!pcspkr_dev)
		return -ENOMEM;

	pcspkr_dev->name = "PC Speaker";
	pcspkr_dev->phys = "isa0061/input0";
	pcspkr_dev->id.bustype = BUS_ISA;
	pcspkr_dev->id.vendor = 0x001f;
	pcspkr_dev->id.product = 0x0001;
	pcspkr_dev->id.version = 0x0100;
	pcspkr_dev->dev.parent = &dev->dev;

	pcspkr_dev->evbit[0] = BIT_MASK(EV_SND);
	pcspkr_dev->sndbit[0] = BIT_MASK(SND_BELL) | BIT_MASK(SND_TONE);
	pcspkr_dev->event = pcspkr_event;

	err = input_register_device(pcspkr_dev);
	if (err) {
		input_free_device(pcspkr_dev);
		return err;
	}
	
	spkr_state = kzalloc(sizeof(*spkr_state), GFP_KERNEL);
	if (!spkr_state) {
		input_free_device(pcspkr_dev);
		return -ENOMEM;
	}

	spkr_state->pcspkr_dev = pcspkr_dev;

	platform_set_drvdata(dev, pcspkr_dev);
	input_set_drvdata(pcspkr_dev, spkr_state);
	
	return 0;
}

static int __devexit pcspkr_remove(struct platform_device *dev)
{
	struct pcspkr_state *spkr_state = platform_get_drvdata(dev);

	/* turn off the speaker */
	pcspkr_event(spkr_state->pcspkr_dev, EV_SND, SND_TONE, 0);
	
	input_unregister_device(spkr_state->pcspkr_dev);
	platform_set_drvdata(dev, NULL);
	kfree(spkr_state);

	return 0;
}

static int pcspkr_suspend(struct platform_device *dev, pm_message_t pmm)
{
	struct pcspkr_state *spkr_state = platform_get_drvdata(dev);
	pcspkr_event(spkr_state->pcspkr_dev, EV_SND, SND_TONE, 0);

	return 0;
}

static int pcspkr_resume(struct platform_device *dev)
{
        struct pcspkr_state *spkr_state = platform_get_drvdata(dev);
        pcspkr_event(spkr_state->pcspkr_dev, EV_SND, SND_TONE, current_value);
	return 0;
}

static void pcspkr_shutdown(struct platform_device *dev)
{
	/* turn off the speaker */
	struct pcspkr_state *spkr_state = platform_get_drvdata(dev);
	pcspkr_event(spkr_state->pcspkr_dev, EV_SND, SND_TONE, 0);
}

static struct platform_driver pcspkr_platform_driver = {
	.driver		= {
		.name	= "pcspkr",
		.owner	= THIS_MODULE,
	},
	.probe		= pcspkr_probe,
	.remove		= __devexit_p(pcspkr_remove),
	.shutdown	= pcspkr_shutdown,
        .suspend        = pcspkr_suspend,
	.resume		= pcspkr_resume,
};


static int __init pcspkr_init(void)
{
	return platform_driver_register(&pcspkr_platform_driver);
}

static void __exit pcspkr_exit(void)
{
	platform_driver_unregister(&pcspkr_platform_driver);
}

module_init(pcspkr_init);
module_exit(pcspkr_exit);
