/*
 * Copyright 2010 TTTECH Computertechnik AG. All Rights Reserved.
 *
 * Code derived from pmic_battery.c by Freescale Semiconductor, Inc.
 *
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

/*
 * Includes
 */
#include <linux/platform_device.h>
#include <linux/power_supply.h>

#include <linux/delay.h>
#include <asm/mach-types.h>
#include <linux/ioctl.h>
#include <linux/pmic_status.h>
#include <linux/pmic_external.h>
#include <linux/pmic_pwr_buttons.h>
#include <linux/input.h>

#include "../core/pmic.h"

struct mc13892_pb_dev_info {
	struct device *dev;
};

extern unsigned int active_events[MAX_ACTIVE_EVENTS];
static struct input_dev *pwr_buttons_dev;

static void buttons_event_callback(void *para)
{
	int status;
	int i;
		
	pmic_read(REG_INT_SENSE1, &status);
	
	for (i=0;i<MAX_ACTIVE_EVENTS;i++)
	{
		switch (active_events[i]) {
			// power-on button
			case EVENT_PWRONIII:
				input_report_switch(pwr_buttons_dev, SW_DOCK, !(status & 0x04));
				break;

			// K15
			case EVENT_PWRONII:
				input_report_switch(pwr_buttons_dev, SW_RFKILL_ALL, !(status & 0x10));
				break;

			// Wakeup
			case EVENT_PWRONI:
				input_report_switch(pwr_buttons_dev, SW_LID, !(status & 0x08));
				break;
			default:
				break;
		}
	}

	input_sync(pwr_buttons_dev);	
}

static int pmic_power_buttons_probe(struct platform_device *pdev)
{
	struct mc13892_pb_dev_info *di;
	pmic_event_callback_t pb_event_callback;
	int error = 0;
	int status = 0;

	di = kzalloc(sizeof(*di), GFP_KERNEL);
	if (!di) {
		return -ENOMEM;
	}

	platform_set_drvdata(pdev, di);

	di->dev	= &pdev->dev;

	pb_event_callback.func = buttons_event_callback;
	pb_event_callback.param = (void *) di;
	pmic_event_subscribe(EVENT_PWRONI, pb_event_callback);
	pmic_event_subscribe(EVENT_PWRONII, pb_event_callback);
	pmic_event_subscribe(EVENT_PWRONIII, pb_event_callback);

	pwr_buttons_dev = input_allocate_device();
	if (!pwr_buttons_dev) {
		dev_dbg(di->dev, "%s input_allocate_device error\n", __func__);
		error = -ENOMEM;
		goto err_free_mem;
	}

	pwr_buttons_dev->name = pdev->name;
	pwr_buttons_dev->phys = "pmic_power_buttons/input0";
	pwr_buttons_dev->dev.parent = &pdev->dev;

	pwr_buttons_dev->id.bustype = BUS_HOST;
	pwr_buttons_dev->id.vendor = 0x0001;
	pwr_buttons_dev->id.product = 0x0001;
	pwr_buttons_dev->id.version = 0x0100;


	//set_bit(EV_KEY, pwr_buttons_dev->evbit);
	set_bit(EV_SW , pwr_buttons_dev->evbit);

	set_bit(SW_RFKILL_ALL, pwr_buttons_dev->swbit);
	set_bit(SW_LID, pwr_buttons_dev->swbit);

	error = input_register_device(pwr_buttons_dev);
	if (error) {
		dev_dbg(di->dev, "%s Failed to register device\n", __func__);
		goto err_free_dev;
	}

	// Report initial status of keys
	pmic_read(REG_INT_SENSE1, &status);
	input_report_switch(pwr_buttons_dev, SW_RFKILL_ALL, !(status & 0x10));
	input_report_switch(pwr_buttons_dev, SW_LID, !(status & 0x08));

	dev_dbg(di->dev, "%s power buttons probed!\n", __func__);
	return 0;

 err_free_dev:
	input_free_device(pwr_buttons_dev);
 err_free_mem:
	kfree(di);
	return error;
}

static struct platform_driver pmic_power_buttons_driver_ldm = {
	.driver = {
		   .name = "pmic_power_buttons",
		   .bus = &platform_bus_type,
		   },
	.probe = pmic_power_buttons_probe,
	.remove = NULL,
};

static int __init pmic_power_buttons_init(void)
{
	pr_debug("PMIC power_buttons driver loading...\n");
	return platform_driver_register(&pmic_power_buttons_driver_ldm);
}

static void __exit pmic_power_buttons_exit(void)
{
	platform_driver_unregister(&pmic_power_buttons_driver_ldm);
	pr_debug("PMIC power_buttons driver successfully unloaded\n");
}

module_init(pmic_power_buttons_init);
module_exit(pmic_power_buttons_exit);

MODULE_DESCRIPTION("pmic power buttons driver");
MODULE_AUTHOR("Klaus Steinhammer <kst@tttech.com>");
MODULE_LICENSE("GPL");
