/*
 * Copyright 2008-2009 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

/*!
 * @defgroup Framebuffer Framebuffer Driver for SDC and ADC.
 */

/*!
 * @file mxcfb_NEC_NL6448BC26-09C.c
 *
 * @brief Initialisation driver for VAC via LVDS port of Vision2
 *
 * @ingroup Framebuffer
 */

/*!
 * Include files
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/console.h>
#include <linux/delay.h>
#include <linux/errno.h>
#include <linux/fb.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/mxcfb.h>
#include <linux/regulator/consumer.h>
#include <mach/hardware.h>
#include <mach/gpio.h>
#include "../../../arch/arm/mach-mx5/iomux.h"
#include "../../../arch/arm/mach-mx5/mx51_pins.h"

static void lvds_poweron(void);
static void lvds_poweroff(void);

static int lvds_on;

static struct fb_videomode video_modes[] = {
{
   	"LVDS-VAC", 60, 640, 480, 
		37650, //pixclock = 26.56Mhz
		48, //left margin
		16, //right margin
		31, //upper margin
		12, //lower margin
		96, //hsync-len 
		2, //vsync-len
	 	0, //sync FB_SYNC_CLK_LAT_FALL
	 	FB_VMODE_NONINTERLACED,
		0,}
};


static void lvds_init_fb(struct fb_info *info)
{
	struct fb_var_screeninfo var;

	printk(KERN_INFO "mxcfb_LVDS_VAC\n");

	memset(&var, 0, sizeof(var));

	fb_videomode_to_var(&var, &video_modes[0]);

	var.activate = FB_ACTIVATE_ALL;
	var.yres_virtual = var.yres * 2;

	acquire_console_sem();
	info->flags |= FBINFO_MISC_USEREVENT;
	fb_set_var(info, &var);
	info->flags &= ~FBINFO_MISC_USEREVENT;
	release_console_sem();
}

static int lvds_fb_event(struct notifier_block *nb, unsigned long val, void *v)
{
	struct fb_event *event = v;

	if (strcmp(event->info->fix.id, "DISP3 BG - DI1")) {
		return 0;
	}

	switch (val) {
	case FB_EVENT_FB_REGISTERED:
		lvds_init_fb(event->info);
		lvds_poweron();
		break;
	case FB_EVENT_BLANK:
		if ((event->info->var.xres != 640) ||
		    (event->info->var.yres != 480)) {
			break;
		}
		if (*((int *)event->data) == FB_BLANK_UNBLANK) {
			lvds_poweron();
		} else {
			lvds_poweroff();
		}
		break;
	}
	return 0;
}

static struct notifier_block nb = {
	.notifier_call = lvds_fb_event,
};

static int __devinit lvds_probe(struct platform_device *pdev)
{
	int i;

	for (i = 0; i < num_registered_fb; i++) {
		if (strcmp(registered_fb[i]->fix.id, "DISP3 BG - DI1") == 0) {
			lvds_init_fb(registered_fb[i]);
			fb_show_logo(registered_fb[i], 0);
			lvds_poweron();
		} else if (strcmp(registered_fb[i]->fix.id, "DISP3 FG") == 0) {
			lvds_init_fb(registered_fb[i]);
		}
	}

	fb_register_client(&nb);

	return 0;
}

static int __devexit lvds_remove(struct platform_device *pdev)
{
	fb_unregister_client(&nb);
	lvds_poweroff();

	return 0;
}

#ifdef CONFIG_PM
static int lvds_suspend(struct platform_device *pdev, pm_message_t state)
{
	return 0;
}

static int lvds_resume(struct platform_device *pdev)
{
	return 0;
}
#else
#define lvds_suspend NULL
#define lvds_resume NULL
#endif

/*!
 * platform driver structure for LVDS_VAC
 */
static struct platform_driver lvds_driver = {
	.driver = {
		   .name = "lcd_LVDS_VAC"},
	.probe = lvds_probe,
	.remove = __devexit_p(lvds_remove),
	.suspend = lvds_suspend,
	.resume = lvds_resume,
};

static void lvds_poweron(void)
{
	if (lvds_on)
		return;
	
	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_DI1_D0_CS), 1);	/* set LVDS_EN */
	msleep(10);
		
	lvds_on = 1;
}

static void lvds_poweroff(void)
{
	lvds_on = 0;

	gpio_set_value(IOMUX_TO_GPIO(MX51_PIN_DI1_D0_CS), 0);	/* clear LVDS_EN */

}

static int __init lvds_vac_init(void)
{
	return platform_driver_register(&lvds_driver);
}

static void __exit lvds_vac_exit(void)
{
	platform_driver_unregister(&lvds_driver);
}

module_init(lvds_vac_init);
module_exit(lvds_vac_exit);

MODULE_AUTHOR("TTControl GmbH");
MODULE_DESCRIPTION("VideoAdapterCard via LDVS port driver for Vision2");
MODULE_LICENSE("GPL");
