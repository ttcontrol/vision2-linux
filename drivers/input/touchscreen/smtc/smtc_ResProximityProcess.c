/*! \file smtc_ResProximityProcess.c
* \brief Code to decode/process proximity on the resistive touch
* controllers
*
* Copyright (c) 2011 Semtech Corp
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License version 2 as
*  published by the Free Software Foundation.
*/


#include <linux/input/smtc/smtc_ResProximityProcess.h>



/*! \fn int smtc_ResProximity_initInput(psmtc_ResProximity_t this,
 * struct device *parent,int bustype)
 * \brief initializer on the input for  proximity
 * \details This does not determine position with proximity, so we want
 * to set this up as a separate input as to not get confused with X/Y
 * \param this Pointer to struct containing information to do with 
 * proximity
 * \param parent Pointer to device struct to pass as parent for input
 * \param busType integer id for the bus type used
 * \return 0 if successful, negative if unsuccessful
*/
int smtc_ResProximity_initInput(psmtc_ResProximity_t this,
                               struct device *parent,int bustype)
{
  struct input_dev *input;
  int error = 0;
  error = -ENOMEM;
	if (this) {
    if (!this->input) {
    	input = input_allocate_device();
      if (input)
        input->name = "proximity";
    } else {
      error = 0; /* tell code to NOT register device allocated elsewhere */
      input = this->input;
    }
    if (input) {
      this->currentData.data = 0;
      input->id.bustype = bustype;
      input->dev.parent = parent;
	    input->evbit[0] |= BIT_MASK(EV_KEY) | BIT_MASK(EV_ABS);
	    set_bit(EV_SYN, input->evbit);

      /* TODO: need to determine max distance */
	    input_set_abs_params(input, ABS_DISTANCE, 0,SMTC_PROX_MAX_DISTANCE, 0, 0);

      if (error) {
  	    error = input_register_device(input);
      } else {
        /* don't register device yet.. user may want to add to this later */
        error = 0;
      }
      if (!error) {
        this->input = input;
        return 0;
      } 
      input_free_device(input);
    }
  }
  return error;
}



/*! \fn void smtc_ResProximity_remove(psmtc_ResProximity_t this)
 * \brief Unregisters input device
 * \param this Pointer to struct containing information to do with 
 * proximity
*/
void smtc_ResProximity_remove(psmtc_ResProximity_t this)
{
  if (this && this->input)
    input_unregister_device(this->input);
}



/*! \fn void smtc_ResProximity_processOutProx(psmtc_ResProximity_t this,s16 data)
 * \brief Processes an out of proximity event (data may or may not be used)
 * \param this Pointer to struct containing information to do with 
 * proximity
 * \param data Information that can be used to determine approximate distances
*/
void smtc_ResProximity_processOutProx(psmtc_ResProximity_t this,s16 data)
{
  /* we only need to send this if we are in proximity */
  if (this && (this->currentData.inProx)) {
    this->currentData.inProx = 0;
    this->currentData.data = data;
    /* Not in proximity, send out of prox event */
    dev_dbg(this->input->dev.parent, "out of proximity: %d\n",data);
    input_report_abs(this->input, ABS_DISTANCE, SMTC_PROX_MAX_DISTANCE);
    input_sync(this->input);
  }
}

/*! \fn void smtc_ResProximity_processInProx(psmtc_ResProximity_t this,s16 data)
 * \brief Processes an in proximity event (data may or may not be used)
 * \param this Pointer to struct containing information to do with 
 * proximity
 * \param data Information that can be used to determine approximate distances
*/
void smtc_ResProximity_processInProx(psmtc_ResProximity_t this,s16 data) 
{
  /* we only need to send this if we are out of proximity */
  if (this && (!this->currentData.inProx)) {
    this->currentData.inProx = 1;
    this->currentData.data = data;
    /* We are in proximity, send data to alert have prox */
   	dev_dbg(this->input->dev.parent, "int proximity useful: %d\n",data);
    /* Right now just use in or out */
    input_report_abs(this->input, ABS_DISTANCE, 0); 
    input_sync(this->input);
  }
}




