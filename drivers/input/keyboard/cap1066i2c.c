/*
 * Copyright 2008-2009 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

/*!
 * @file  linux/drivers/input/keyboard/cap1066i2c.c
 *
 * @brief Driver for the SMSC CAP1066 Touch KeyPad Controller driver connected via i2c.
 *
 *
 *
 * @ingroup Keypad
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/string.h>
#include <linux/bcd.h>
#include <linux/list.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/kthread.h>
#include <linux/input.h>
#include <linux/delay.h>
#include <linux/fsl_devices.h>
#include <linux/regulator/consumer.h>
#include <asm/mach/irq.h>
#include <mach/gpio.h>

/*
 * Definitions
 */
#undef DEBUG

#define KEY_COUNT         8

#undef LED_BREATHE
#define LED_FADE

/*
  *Registers in CAP1066
  */

#define CAP1066_MAIN_STATUS_CONTROL				0x00
#define CAP1066_SENSOR_STATUS					0x03
#define CAP1066_LED_STATUS						0x04
#define CAP1066_NOISE_FLAG_STATUS				0x0A
#define CAP1066_SENSOR_1_DELTA_COUNT			0x10
#define CAP1066_SENSOR_2_DELTA_COUNT			0x11
#define CAP1066_SENSOR_3_DELTA_COUNT			0x12
#define CAP1066_SENSOR_4_DELTA_COUNT			0x13
#define CAP1066_SENSOR_5_DELTA_COUNT			0x14
#define CAP1066_SENSOR_6_DELTA_COUNT			0x15
#define CAP1066_SENSIVITY_CONTROL				0x1F
#define CAP1066_CONFIGURATION					0x20
#define CAP1066_SENSOR_ENABLE					0x21
#define CAP1066_SENSOR_CONFIGURATION			0x22
#define CAP1066_SENSOR_CONFIGURATION_2			0x23
#define CAP1066_AVERAGING_AND_SAMPLIG_CONFIG	0x24
#define CAP1066_CALIBRATION_ACTIVE				0x26
#define CAP1066_INTERRUPT_ENABLE				0x27
#define CAP1066_REPEAT_RATE_ENABLE				0x28
#define CAP1066_MULTIPLE_PRESS_CONFIGURATION	0x2A
#define CAP1066_RECALIBRATION_CONFIGURATION		0x2F
#define CAP1066_SENSOR_1_THRESHOLD				0x30
#define CAP1066_SENSOR_2_THRESHOLD				0x31
#define CAP1066_SENSOR_3_THRESHOLD				0x32
#define CAP1066_SENSOR_4_THRESHOLD				0x33
#define CAP1066_SENSOR_5_THRESHOLD				0x34
#define CAP1066_SENSOR_6_THRESHOLD				0x35
#define CAP1066_SENSOR_NOISE_THRESHOLD_1		0x38
#define CAP1066_SENSOR_NOISE_THRESHOLD_2		0x39
#define CAP1066_STANDBY_CHANNEL					0x40
#define CAP1066_STANDBY_CONFIGURATION			0x41
#define CAP1066_STANDBY_SENSITIVITY				0x42
#define CAP1066_STANDBY_THRESHOLD				0x43
#define CAP1066_SENSOR_1_BASE_COUNT				0x50
#define CAP1066_SENSOR_2_BASE_COUNT				0x51
#define CAP1066_SENSOR_3_BASE_COUNT				0x52
#define CAP1066_SENSOR_4_BASE_COUNT				0x53
#define CAP1066_SENSOR_5_BASE_COUNT				0x54
#define CAP1066_SENSOR_6_BASE_COUNT				0x55
#define CAP1066_LED_OUTPUT_TYPE					0x71
#define CAP1066_SENSOR_LED_LINKING				0x72
#define CAP1066_LED_POLARITY					0x73
#define CAP1066_LED_OUTPUT_CONTROL				0x74
#define CAP1066_LED_BEHAVIOR_1					0x81
#define CAP1066_LED_BEHAVIOR_2					0x82
#define CAP1066_LED_PULSE_1_PERIOD				0x84
#define CAP1066_LED_PULSE_2_PERIOD				0x85
#define CAP1066_LED_BREATHE_PERIOD				0x86
#define CAP1066_LED_CONFIG						0x88
#define CAP1066_LED_PULSE_1_DUTY_CYCLE			0x90
#define CAP1066_LED_PULSE_2_DUTY_CYCLE			0x91
#define CAP1066_LED_BREATHE_DUTY_CYCLE			0x92
#define CAP1066_LED_DIRECT_DUTY_CYCLE			0x93
#define CAP1066_LED_DIRECT_RAMP_RATE			0x94
#define CAP1066_LED_OFF_DELAY					0x95
#define CAP1066_PRODUCT_ID						0xFD
#define CAP1066_MANUFACTURER_ID					0xFE
#define CAP1066_REVISION						0xFF
  
#define CAP1066_ADDR_MINI                        CAP1066_MAIN_STATUS_CONTROL
#define CAP1066_ADDR_MAX                         CAP1066_REVISION

#define DRIVER_NAME "cap1066i2c"

struct cap1066_data {
	struct i2c_client *client;
	struct device_driver driver;
	struct input_dev *idev;
	struct task_struct *tstask;
	struct completion kpirq_completion;
	int kpirq;
	int kp_thread_cnt;
	int opened;
};

static int kpstatus[KEY_COUNT];
static struct mxc_keyp_platform_data *keypad;
static const unsigned short *mxckpd_keycodes;
static struct regulator *vdd_reg;

static int cap1066_read_register(struct cap1066_data *data,
				unsigned char regaddr, int *value)
{
	int ret = 0;
	unsigned char regvalue;

	ret = i2c_master_send(data->client, &regaddr, 1);
	if (ret < 0)
		goto err;
	udelay(20);
	ret = i2c_master_recv(data->client, &regvalue, 1);
	if (ret < 0)
		goto err;
	*value = regvalue;

	return ret;
err:
	return -ENODEV;
}

static int cap1066_write_register(struct cap1066_data *data,
				 u8 regaddr, u8 regvalue)
{
	int ret = 0;
	unsigned char msgbuf[2];

	msgbuf[0] = regaddr;
	msgbuf[1] = regvalue;
	ret = i2c_master_send(data->client, msgbuf, 2);
	if (ret < 0) {
		printk(KERN_ERR "%s - Error in writing to I2C Register %d \n",
		       __func__, regaddr);
		return ret;
	}

	return ret;
}


static irqreturn_t cap1066_keypadirq(int irq, void *v)
{
	struct cap1066_data *d = v;

	disable_irq_nosync(d->kpirq);
	complete(&d->kpirq_completion);
	return IRQ_HANDLED;
}

static int cap1066ts_thread(void *v)
{

	struct cap1066_data *d = v;
	int ret = 0, oldstatus = 0;
	int i = 0, currentstatus = 0, released = 0;

	if (d->kp_thread_cnt)
		return -EINVAL;
	d->kp_thread_cnt = 1;
	while (1) {

		if (kthread_should_stop())
			break;
		/* Wait for keypad interrupt */
		if (wait_for_completion_interruptible_timeout
		    (&d->kpirq_completion, HZ) <= 0)
			continue;
	
		ret = cap1066_read_register(d, CAP1066_SENSOR_STATUS, &currentstatus);
		if (ret < 0) {
			printk(KERN_ERR
			       "%s: Err in reading keypad status register\n",
			       __func__);
		} else {
			msleep(10);
			ret = cap1066_write_register(d, CAP1066_MAIN_STATUS_CONTROL, 0x00);
			if (ret < 0){
				printk(KERN_ERR "%s: Err in writing keypad status register\n",
				       __func__);
			}
			ret = cap1066_read_register(d, CAP1066_SENSOR_STATUS, &released);

//			if (oldstatus != currentstatus) {
				for (i=0;i<=KEY_COUNT;i++) {
					if ( (oldstatus & (1<<i)) != (currentstatus & (1<<i)) ) {
						if (currentstatus & (1<<i)) {
							/* Key pressed. */
							input_event(d->idev, EV_KEY, mxckpd_keycodes[i], 1);
							input_sync(d->idev);
						}
						else {
							/*Key released. */
							input_event(d->idev, EV_KEY, mxckpd_keycodes[i], 0);
							input_sync(d->idev);
						}
					}
					else {
						if ((released & (1<<i)) != (currentstatus & (1<<i)) ) {
							/*Key pressed. */
							input_event(d->idev, EV_KEY, mxckpd_keycodes[i], 1);
							/*Key released. */
							input_event(d->idev, EV_KEY, mxckpd_keycodes[i], 0);
							input_sync(d->idev);
						}
					}
				}
//				input_sync(d->idev);
				oldstatus = currentstatus;
//			}
		}
		/* Re-enable interrupts */
		enable_irq(d->kpirq);
	}

	d->kp_thread_cnt = 0;

	return 0;
}

/*!
 * This function puts the Keypad controller in low-power mode/state.
 *
 * @param   pdev  the device structure used to give information on Keypad
 *                to suspend
 * @param   state the power state the device is entering
 *
 * @return  The function always returns 0.
 */
static int cap1066_suspend(struct i2c_client *client, pm_message_t state)
{
	struct cap1066_data *d = i2c_get_clientdata(client);

	if (!IS_ERR(d->tstask) && d->opened)
		kthread_stop(d->tstask);

	return 0;
}

/*!
 * This function brings the Keypad controller back from low-power state.
 *
 * @param   pdev  the device structure used to give information on Keypad
 *                to resume
 *
 * @return  The function always returns 0.
 */
static int cap1066_resume(struct i2c_client *client)
{
	struct cap1066_data *d = i2c_get_clientdata(client);

	if (d->opened)
		d->tstask = kthread_run(cap1066ts_thread, d, DRIVER_NAME "kpd");

	return 0;
}

static int cap1066_idev_open(struct input_dev *idev)
{
	struct cap1066_data *d = input_get_drvdata(idev);
	int ret = 0;

	d->tstask = kthread_run(cap1066ts_thread, d, DRIVER_NAME "kpd");
	if (IS_ERR(d->tstask))
		ret = PTR_ERR(d->tstask);
	else
		d->opened++;
	return ret;
}

static void cap1066_idev_close(struct input_dev *idev)
{
	struct cap1066_data *d = input_get_drvdata(idev);

	if (!IS_ERR(d->tstask))
		kthread_stop(d->tstask);
	if (d->opened > 0)
		d->opened--;
}

static int cap1066_driver_register(struct cap1066_data *data)
{
	struct input_dev *idev;
	int ret = 0;
	int i;

	if (data->kpirq) {
		ret =
		    request_irq(data->kpirq, cap1066_keypadirq,
				IRQF_TRIGGER_FALLING, DRIVER_NAME, data);
		if (!ret) {
			init_completion(&data->kpirq_completion);
			set_irq_wake(data->kpirq, 1);
		} else {
			printk(KERN_ERR "%s: cannot grab irq %d\n",
			       __func__, data->kpirq);
		}

	}
	idev = input_allocate_device();
	
	data->idev = idev;
	input_set_drvdata(idev, data);
	
	idev->name = DRIVER_NAME;
	idev->phys = "cap1066i2c/input0";
	
	idev->open = cap1066_idev_open;
	idev->close = cap1066_idev_close;
	
	idev->id.bustype = BUS_I2C;
	idev->id.vendor = 0x0001;
	idev->id.product = 0x0002;
	idev->id.version = 0x0100;
	
	idev->keycode = &mxckpd_keycodes;
	idev->keycodesize = sizeof(unsigned char);
	idev->keycodemax = KEY_COUNT;
	idev->id.bustype = BUS_I2C;
	
	set_bit(EV_KEY, idev->evbit);
//	set_bit(EV_REP, idev->evbit);
	
	for (i = 0; i < 8; i++)
		set_bit(mxckpd_keycodes[i], idev->keybit);
	
	if (!ret)
		ret = input_register_device(idev);

	return ret;
}

static int cap1066_i2c_remove(struct i2c_client *client)
{
	struct cap1066_data *d = i2c_get_clientdata(client);

	set_irq_wake(d->kpirq, 0);
	free_irq(d->kpirq, d);
	input_unregister_device(d->idev);
	if (keypad->inactive)
		keypad->inactive();

	/*Disable the Regulator*/
	if (keypad->vdd_reg) {
		regulator_disable(vdd_reg);
		regulator_put(vdd_reg);
	}

	return 0;
}

static int cap1066_configure(struct cap1066_data *data)
{
	int ret = 0;

#if defined LED_BREATHE
	ret = cap1066_write_register(data, CAP1066_LED_BEHAVIOR_1, 0xFF);
	if (ret < 0)
		goto err;
	ret = cap1066_write_register(data, CAP1066_LED_BEHAVIOR_2, 0x0F);
	if (ret < 0)
		goto err;
	ret = cap1066_write_register(data, CAP1066_LED_BREATHE_PERIOD, 0x14);
	if (ret < 0)
		goto err;
#else 
	ret = cap1066_write_register(data, CAP1066_LED_BEHAVIOR_1, 0x00);
	if (ret < 0)
		goto err;
	ret = cap1066_write_register(data, CAP1066_LED_BEHAVIOR_2, 0x00);
	if (ret < 0)
		goto err;
#if defined LED_FADE
	ret = cap1066_write_register(data, CAP1066_LED_DIRECT_RAMP_RATE, 0x0F);
	if (ret < 0)
		goto err;
#else
	ret = cap1066_write_register(data, CAP1066_LED_DIRECT_RAMP_RATE, 0x00);
	if (ret < 0)
		goto err;
#endif	
#endif

	ret = cap1066_write_register(data, CAP1066_SENSOR_LED_LINKING, 0x3F);
	if (ret < 0)
		goto err;

	/* set configuration */
	ret = cap1066_write_register(data, CAP1066_CONFIGURATION, 0x28);
	if (ret < 0)
		goto err;
	
	/* clear all interrupts */		
	ret = cap1066_write_register(data, CAP1066_MAIN_STATUS_CONTROL, 0x00);
	if (ret < 0)
		goto err;

	/* recalibrate keys */
	ret = cap1066_write_register(data, CAP1066_CALIBRATION_ACTIVE, 0x3f);
	if (ret < 0)
		goto err;

	return ret;
err:
	return -ENODEV;
}

static int cap1066_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct cap1066_data *data;
	int err = 0;
        #ifdef DEBUG
        i = 0;
        #endif
	int regValue = 0;

	data = kzalloc(sizeof(struct cap1066_data), GFP_KERNEL);
	if (data == NULL)
		return -ENOMEM;
	i2c_set_clientdata(client, data);
	data->client = client;
	data->kpirq = client->irq;
	
	keypad = (struct mxc_keyp_platform_data *)(client->dev).platform_data;
	if (keypad->active)
		keypad->active();

	/*Enable the Regulator*/
	if (keypad && keypad->vdd_reg) {
		vdd_reg = regulator_get(&client->dev, keypad->vdd_reg);
		if (!IS_ERR(vdd_reg))
			regulator_enable(vdd_reg);
		else
			vdd_reg = NULL;
	} else
		vdd_reg = NULL;

	mxckpd_keycodes = keypad->matrix;

	err = cap1066_driver_register(data);
	if (err < 0)
		goto exit_free;

	err = cap1066_read_register(data, CAP1066_MANUFACTURER_ID, &regValue);
	if ((err == -ENODEV) || (regValue != 0x5D)) {
		printk(KERN_ERR "%s: Err reading manufacturer ID\n", __func__);
		goto exit_unregister;
	}
	err = cap1066_read_register(data, CAP1066_PRODUCT_ID, &regValue);
	if ((err == -ENODEV) || (regValue != 0x41)) {
		printk(KERN_ERR "%s: Err reading device ID\n", __func__);
		goto exit_unregister;
	}
	err = cap1066_read_register(data, CAP1066_REVISION, &regValue);
		printk(KERN_INFO "%s: got CAP1066 revision %02x\n", __func__, regValue);

	err = cap1066_configure(data);
	if (err == -ENODEV) {
		goto exit_unregister;
	}

#if defined DEBUG
	for (i = CAP1066_ADDR_MINI; i <= CAP1066_ADDR_MAX; i++) {
		err = cap1066_read_register(data, i, &regValue);
		if (err < 0) {
		   printk(KERN_ERR
		  "%s: Err in reading keypad CONFIGADDR register\n",
		   __func__);
		   goto exit_unregister;
	    }
	    printk("CAP1066 Register id: %02x, Value:%02x \n", i, regValue);

	}
#endif

	memset(kpstatus, 0, sizeof(kpstatus));
	printk(KERN_INFO "%s: Device Attached\n", __func__);
	return 0;
exit_unregister:
	free_irq(data->kpirq, data);
	input_unregister_device(data->idev);
exit_free:
	/*disable the Regulator*/
	if (vdd_reg) {
		regulator_disable(vdd_reg);
		regulator_put(vdd_reg);
		vdd_reg = NULL;
	}
	kfree(data);
	return err;
}

static const struct i2c_device_id cap1066_id[] = {
	{ "cap1066i2c", 0 },
	{},
};
MODULE_DEVICE_TABLE(i2c, cap1066_id);

static struct i2c_driver cap1066_driver = {
	.driver = {
		   .name = DRIVER_NAME,
		   },
	.probe = cap1066_i2c_probe,
	.remove = cap1066_i2c_remove,
	.suspend = cap1066_suspend,
	.resume = cap1066_resume,
	.command = NULL,
	.id_table = cap1066_id,
};
static int __init cap1066_init(void)
{
	return i2c_add_driver(&cap1066_driver);
}

static void __exit cap1066_exit(void)
{
	i2c_del_driver(&cap1066_driver);
}

MODULE_AUTHOR("Klaus Steinhammer <kst@tttech.com>");
MODULE_DESCRIPTION("SMSC 1066 Touch KeyPad Controller driver");
MODULE_LICENSE("GPL");
module_init(cap1066_init);
module_exit(cap1066_exit);
