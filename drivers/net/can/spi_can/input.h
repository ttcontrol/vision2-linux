#ifndef INPUT_H
#define INPUT_H

#include "globals.h"
#include <linux/input.h>

#define INPUT_KEY_FORWARD  KEY_UP
#define INPUT_KEY_BACKWARD KEY_DOWN
#define INPUT_KEY_CONFIRM  KEY_ENTER

int  input_key_event(ubyte2 _code, ubyte1 _value);
int  input_init(void);
void input_exit(void);

#endif

