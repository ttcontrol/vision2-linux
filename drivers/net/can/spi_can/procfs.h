#ifndef PROCFS_H
#define PROCFS_H

#include "globals.h"
#include <linux/netdevice.h>
#include "protocol.h"

#define PROCFS_CAN_ROOT_NAME    "spi_can"
#define PROCFS_BITTIMING_NAME   "bittiming"
#define PROCFS_FILTERING_NAME   "filtering"
#define PROCFS_STATS_NAME       "stats"
#define PROCFS_INFO_NAME        "info"

#define PROCFS_LED_ROOT_NAME    "spi_leds"
#define PROCFS_LED_A_VALUE_NAME "a"
#define PROCFS_LED_B_VALUE_NAME "b"

void procfs_create_root(void);
void procfs_remove_root(void);
void procfs_add_device(ubyte1 _dev);
void procfs_remove_device(ubyte1 _dev);

extern struct spi_can_status_t spiCanStatus[SPI_ENDP_CAN_MAX];
extern struct xc_device_info_t xcDeviceInfo;
#endif
