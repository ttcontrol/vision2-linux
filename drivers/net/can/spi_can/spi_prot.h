#ifndef SPI_PROT_H
#define SPI_PROT_H

#include <linux/can.h>
#include "globals.h"
#include "spi_can.h"
#include "protocol.h"

#define SPI_OK              0   // Everything is OK
#define SPI_ERR_INVALIDDEV -1   // The specified device is unknown/invalid
#define SPI_ERR_SPI        -2   // An SPI-specific error occurred
#define SPI_ERR_BUF_EMPTY  -3   // The requested buffer is empty (e.g., read-buffer)
#define SPI_ERR_BUF_FULL   -4   // The requested buffer is full (e.g., write-buffer)
#define SPI_ERR_NULL_PTR   -5   // A NULL-pointer has been encountered
#define SPI_ERR_PARAMETER  -6   // One of the given parameters is invalid
#define SPI_ERR_CALLBACK   -7   // The receive-callback-function returned an error
#define SPI_ERR_KERNEL     -8   // An error occured in an API-function
#define SPI_ERR_COM_INIT   -9   // The communication interface is not initialized

/****************************************************************************//**
 * \brief Defines the callback-function that is called whenever a CAN-message
 *        has been received.
 *
 * \remarks Parameter spi_can_devs_t specifies which CAN-channel has received
 *          the respective message.
 * \remarks Parameter can_frame is a pointer to the contents of the message.
 ********************************************************************************/
typedef int (*spi_prot_CANRead_t)(ubyte1, const struct can_frame *const);

void spi_prot_processMessage(const struct spi_frame_t *const _frame);
int  spi_prot_CANWrite(ubyte1 _dev, const struct can_frame *const _frame);
int  spi_prot_CANConfigure(ubyte1 _dev, ubyte4 _baudrate, ubyte1 _tseg1, ubyte1 _tseg2, ubyte1 _sjw, bool _enabled);
int  spi_prot_CANStatus(ubyte1 _dev, struct spi_can_status_t *const _status);
int  spi_prot_CANReadCallback(spi_prot_CANRead_t _callback);
int  spi_prot_EncoderConfigure(ubyte1 _inc, ubyte1 _thres, ubyte1 _repeat, ubyte1 _enabled);
int  spi_prot_Reset(void);
int  spi_prot_LED_SetBrightness(ubyte1 _green, ubyte1 _red);
int  spi_prot_LED_GetBrightness(ubyte1 *_green, ubyte1 *_red);
int  spi_prot_probe(void);
void spi_prot_remove(void);
void spi_prot_suspend(void);
void spi_prot_resume(void);

#endif
