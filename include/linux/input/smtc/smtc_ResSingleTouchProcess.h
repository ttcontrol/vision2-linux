
#ifndef _SMTC_RESSINGLETOUCHPROCESS_H_
#define _SMTC_RESSINGLETOUCHPROCESS_H_


#include <linux/slab.h>
#include <linux/input.h>

#define TOTAL_CHANNELS_AVAILABLE 4

#include "smtc_ResSingleTouch_platform_data.h"

/* analog channels - used when reading back data */
#define SMTC_CH_X	  0
#define SMTC_CH_Y	  1
#define SMTC_CH_Z1	2
#define SMTC_CH_Z2	3
#define SMTC_CH_AUX	4
#define SMTC_CH_SEQ	7

/* depending on the orientation of the touchscreen, we may
 * need to modify (flip the axis). Another option is to
 * use SWAPXY
 */
#define FLIPX
#define FLIPY



struct smtc_resSTData
{
  u16 x;
  u16 y;
  u16 z1;
  u16 z2;
};

struct smtc_resSTDataLarge
{
  u32 x;
  u32 y;
  u32 z1;
  u32 z2;
};

struct smtc_ResSingleTouch
{
  struct input_dev             *input;
 
  u8 newData;
 
  struct smtc_resSTData         currentData;

  psmtc_ResSingleTouch_platform_data_t pPlatformData;

} typedef smtc_ResSingleTouch_t, *psmtc_ResSingleTouch_t;


int smtc_ResSingleTouch_initInput(psmtc_ResSingleTouch_t this, 
                                  psmtc_ResSingleTouch_platform_data_t pdata,
                                  struct device *parent,int bustype);

int smtc_ResSingleTouch_decode(psmtc_ResSingleTouch_t this,
                                  u8 *buffer, int size);
	
void smtc_ResSingleTouch_processNoTouch(psmtc_ResSingleTouch_t this);


void smtc_ResSingleTouch_processTouch(psmtc_ResSingleTouch_t this);

void smtc_ResSingleTouch_remove(psmtc_ResSingleTouch_t this);



#endif // _SMTC_RESSINGLETOUCHPROCESS_H_


