/*
 * Copyright 2010 TTTECH Computertechnik AG. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU Lesser General
 * Public License.  You may obtain a copy of the GNU Lesser General
 * Public License Version 2.1 or later at the following locations:
 *
 * http://www.opensource.org/licenses/lgpl-license.html
 * http://www.gnu.org/copyleft/lgpl.html
 */

#ifndef __ASM_ARCH_MXC_PMIC_PB_H__
#define __ASM_ARCH_MXC_PMIC_PB_H__

#define SIG_PWR_BUTTON_EVENT	44

#define SIG_PWR_BUTTON_1		1
#define SIG_PWR_BUTTON_2		2
#define SIG_PWR_BUTTON_3		4

#endif				/* __ASM_ARCH_MXC_PMIC_PB_H__ */
