/*
 * MAX9526 platform data
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef __LINUX_I2C_TVP5150_H__
#define __LINUX_I2C_TVP5150_H__

struct tvp5150_platform_data {
	unsigned int resetb_pin;
	unsigned int pdn_pin;
	unsigned int csi;
	
};

#endif
